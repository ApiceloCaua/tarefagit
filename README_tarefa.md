# Processo Seletivo - IN Junior 🐺

## Tarefa de GIT - Cauã Apicelo

- Criação de um mini resumo de cada comando aprendido durante as aulas de git, podendo ser utilizado para futuras consultas. 

- Usando commit para as alterações que foram feitas. 

- Usando as boas práticas (padronização das mensagens dos commits).

# Fontes/documentações utilizadas:

Documentação do git:
https://git-scm.com/doc

Playlist GIT:
https://www.youtube.com/playlist?list=PLxjCnO-f9JPT6Kuww_d2TJQK31Mz-cwR4

Roteiro de Comandos Git da Aula - Material de Apoio

Mais Comandos GIT - Material de Apoio

Repositório remoto público: 

https://gist.github.com/leocomelli/2545add34e4fec21ec16#atualizar-reposit%C3%B3rio-local-de-acordo-com-o-reposit%C3%B3rio-remoto

Boas práticas GIT: 

https://dev.to/vitordevsp/padronizacao-de-commit-com-commitlint-husky-e-commitizen-3g1n

https://www.dio.me/articles/git-boas-praticas-para-escrita-das-mensagens-de-commits

